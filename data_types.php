<?php

$myBooleanVar = true;

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}



$myBooleanVar = false;

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}



$myBooleanVar = "1324545";

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}


$myBooleanVar = "-1324545";

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}



$myBooleanVar = "";

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}



$myBooleanVar = null;

if($myBooleanVar)
{
    echo '$myBooleanVar is True<br>';
}
else
{
    echo '$myBooleanVar is False<br>';
}














//string example start

    $myVar = 4854;

    $myStr = "Hello World! $myVar <br>";

        echo $myStr;

    $myStr = 'Hello World! $myVar <br>';

        echo $myStr;



$myStr = 'Hello World! 12345 <br>';
$heredocVar = <<<BITM
    hsoh osufoi  iuy  iuyf hoiys oi
    ihysf
    soidyf oiysr h $myStr
    uyt ry
BITM;

echo $heredocVar   . "<br>";



$myStr = 'Hello World! 12345 <br>';
$nowdocVar = <<<'BITM'
    hsoh osufoi  iuy  iuyf hoiys oi
    ihysf
    soidyf oiysr h $myStr
    uyt ry
BITM;

echo $nowdocVar   . "<br>";

//string example end












//array example start here
    $myArray = array(13, true, 75.25,"Hello", 5);

    echo "<pre>";

        var_dump ($myArray);

        print_r ($myArray);

    echo "</pre>";


    $personAge = array("Rahim"=>35,"Karim"=>15, "Jorina"=>55, "Mamun"=>85, 50.5);

         echo $personAge ['Jorina']."<br>";

            print_r($personAge);

         echo "</pre>";
//array example start here