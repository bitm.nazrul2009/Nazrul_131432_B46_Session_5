<?php

    define ("BITM", "BASIS Institute of Technology & Management");

    echo BITM.__LINE__."<br>";

    echo __FILE__."<br>";

function doSomething(){
    echo __FUNCTION__ . "<br>";
}
doSomething();

class MyClass{
    public $className;
    public function myMethod(){

        $this->className = __CLASS__;
        echo __METHOD__ . "<br>";
    }
}
$myObject = new MyClass();

$myObject->myMethod();
echo $myObject->className."<br>";

echo __NAMESPACE__;

$strl = "string 1";
$str2 = "string 2";


$str2 .= "str1";

    echo $str2;